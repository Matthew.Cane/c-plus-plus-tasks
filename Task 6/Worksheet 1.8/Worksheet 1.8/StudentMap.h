#pragma once
#include <map>
#include <string>
#include <iostream>
using namespace std;

struct info_t {
	string name;
	int age;
	int tel;
	string courseCode;
};

class StudentMap {
private:
	map<int, info_t> studentMap;
public:
	void run();
	void addStudent();
	void removeStudent();
	void printMap();
	void clearMap();
	void inputFail(string failMessage);
};
