#include "StudentMap.h"

void StudentMap::run() {
	int option;

	cout << "# Matt's Student Database" << endl;
	cout << "#" << endl;
	while (true) {
		cout << "#" << endl;
		cout << "# What task would you like to perform:" << endl;
		cout << "#     0. Exit the program" << endl;
		cout << "#     1. Add a student" << endl;
		cout << "#     2. Remove a student" << endl;
		cout << "#     3. Print all students" << endl;
		cout << "#     4. Clear student database" << endl;
		cout << "#" << endl;
		cout << "# Enter Option between 0 - 4: ";
		cin >> option;
		if (cin.fail()) {
			inputFail("# Incorrect input, please enter a number between 1 - 4");
			continue;
		}

		switch (option) {
		case 0:
			exit(0);
		case 1: addStudent(); break;
		case 2:	removeStudent(); break;
		case 3:	printMap(); break;
		case 4:	clearMap(); break;
		default: cout << "# Incorrect input, please enter a number between 0 - 4" << endl; break;
		}
	}
};

void StudentMap::addStudent() {
	int sNum;
	string name;
	int age;
	int tel;
	string courseCode;
	info_t studentInfo;
	map<int, info_t>::iterator it;

	while (true) {
		cout << "# Enter Students Number: ";
		cin >> sNum;
		if (cin.fail()) {
			inputFail("# Invalid Student Number");
			continue;
		}
		break;
	}
	if (studentMap.count(sNum) != 0) {
		cout << "# Student with this ID already exists" << endl << "# " << endl;
		return;
	}
	while (true) {
		cout << "# Enter Students Name: ";
		cin >> name;
		if (cin.fail()) {
			inputFail("# Invalid Student Name");
			continue;
		}
		break;
	}
	while (true) {
		cout << "# Enter Students Age: ";
		cin.clear();
		cin.ignore();
		cin >> age;
		if (cin.fail()) {
			inputFail("# Invalid Student Age");
			continue;
		}
		break;
	}
	while (true) {
		cout << "# Enter Students Telephone Number: ";
		cin >> tel;
		if (cin.fail()) {
			inputFail("# Invalid Telephone Number");
			continue;
		}
		break;
	}
	while (true) {
		cout << "# Enter Students Course Code: ";
		cin >> courseCode;
		if (cin.fail()) {
			inputFail("# Invalid Course Code");
			continue;
		}
		break;
	}
	cout << "# Information being entered; " << endl;
	cout << "#     ID: " << sNum << endl;
	cout << "#     Name: " << name << endl;
	cout << "#     Age: " << age << endl;
	cout << "#     Telephone Number: " << tel << endl;
	cout << "#     Course Code: " << courseCode << endl;
	cout << "#" << endl;

	studentInfo.name = name;
	studentInfo.age = age;
	studentInfo.tel = tel;
	studentInfo.courseCode = courseCode;

	studentMap.insert({ sNum, studentInfo });

	cout << "# Student successfully entered" << endl;
}
void StudentMap::removeStudent() {
	int sNum;
	while (true) {
		cout << "# Enter number of student to be removed: ";
		cin >> sNum;
		if (cin.fail()) {
			inputFail("# Invalid Student Number");
			continue;
		}
		break;
	}
	if (studentMap.count(sNum) != 0) {
		cout << "# Student with this ID does not exist" << endl << "# " << endl;
		return;
	}
	studentMap.erase(sNum);
	cout << "# Student " << sNum << " removed from database" << endl << "# " << endl;
}
void StudentMap::printMap() {
	map<int, info_t>::iterator it;

	cout << "# Printing " << studentMap.size() << " database entries:" << endl;
	cout << "#" << endl;

	for (it = studentMap.begin(); it != studentMap.end(); it++)	{

		cout << "# Student ID Number: " << it->first << endl;;
		cout << "#     Name: " << it->second.name << endl;
		cout << "#     Age: " << it->second.age << endl;
		cout << "#     Telephone Number: " << it->second.tel << endl;
		cout << "#     Course Code: " << it->second.courseCode << endl;
		cout << "#" << endl;
	}
}
void StudentMap::clearMap() {
	cout << "# Erasing " << studentMap.size() << " database entries..." << endl;
	studentMap.clear();
	cout << "# Database erased" << endl << "#" << endl;
}

void StudentMap::inputFail(string failMessage) {
	cout << failMessage << endl;
	cin.clear();
	cin.ignore(1000, '\n');
}