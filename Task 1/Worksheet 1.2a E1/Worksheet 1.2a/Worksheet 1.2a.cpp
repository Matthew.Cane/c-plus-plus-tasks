#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
	int score;
	cout << "Input your score: ";
	cin >> score;
	if (cin.fail() || score < 0 || score > 100) {
		cout << "Inputted score is out of range or incorrect. The score must be a number between 0 and 100. Program terminating";
		return 0;
	}
	cout << "Your grade is: ";
	if (score < 60) {
		cout << "F";
		return 0;
	}
	else if (score < 70) {
		cout << "D";
		return 0;
	}
	else if (score < 80) {
		cout << "C";
		return 0;
	}
	else if (score < 90) {
		cout << "B";
		return 0;
	}
	else if (score < 101) {
		cout << "A";
		return 0;
	}
}
