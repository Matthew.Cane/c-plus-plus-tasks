#pragma once
#include <string>
#include <time.h>
using namespace std;

class Bunny {
private:
	int bunnyID;
	string sex;
	string colour;
	int age;
	string name;

	Bunny *nextBunny;

public:
	Bunny(int bunnyID, Bunny *nextBunny);

	int getID();
	string getSex();
	string getColour();
	int getAge();
	string getName();

	void setNext(Bunny *next);
	Bunny* getNext();
};