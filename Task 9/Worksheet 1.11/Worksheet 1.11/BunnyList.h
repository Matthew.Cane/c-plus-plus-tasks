#pragma once
#include "Bunny.h"
#include <iostream>
using namespace std;

class BunnyList {
private:
	Bunny *head;

public:
	BunnyList();
	~BunnyList();

	int insert(int BunnyID);
	bool remove(int);
	Bunny* getBunny(int bunnyID);
	void printBunny(int bunnyID);
};