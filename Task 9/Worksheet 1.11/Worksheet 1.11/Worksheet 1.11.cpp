#include <iostream>
#include "BunnyList.h"
#include "Bunny.h"
using namespace std;

int main()
{
	srand(time(0));

	BunnyList list;

	for (int i = 0; i < 10; i++) {
		int bun = list.insert(i);
	}
	cout << endl;

	list.remove(4);

	for (int i = 0; i < 10; i++) {
		list.printBunny(i);
	}

	return 0;
}
