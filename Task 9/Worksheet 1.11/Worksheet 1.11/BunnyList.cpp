#include "BunnyList.h"

BunnyList::BunnyList() {
	head = NULL;
}

BunnyList::~BunnyList() {
	Bunny* current = head;
	Bunny* next = NULL;

	while (current != NULL)
	{
		next = current->getNext();
		delete current;
		current = next;
	}
}

int BunnyList::insert(int bunnyID) {
	Bunny *newBunny_p = new Bunny(bunnyID, NULL);
	newBunny_p->setNext(head);
	head = newBunny_p;
	cout << "Created Bunny with ID " << bunnyID << endl;
	return bunnyID;
}

bool BunnyList::remove(int bunnyID) {
	Bunny* current = head;
	Bunny* previous = NULL;
	Bunny* next = NULL;

	while (current != NULL) {
		if (current->getID() == bunnyID) {
			next = current->getNext();
			previous->setNext(next);
			delete current;
			cout << "Deleted Bunny with ID " << bunnyID << endl;
			return true;
		}
		previous = current;
		current = current->getNext();
	}
	return NULL;
}

Bunny* BunnyList::getBunny(int bunnyID) {
	Bunny *current = head;
	Bunny *previous = NULL;

	while (current != NULL) {
		if (current->getID() == bunnyID) {
			return current;
		}
		previous = current;
		current = current->getNext();
	}
	return NULL;
}

void BunnyList::printBunny(int bunnyID) {
	Bunny* foundBunny = getBunny(bunnyID);

	if (foundBunny) {
		cout << "\tPrinting bunny with ID: " << foundBunny->getID() << endl;
		cout << "\tName: " << foundBunny->getName() << endl;
		cout << "\tAge: " << foundBunny->getAge() << endl;
		cout << "\tColour: " << foundBunny->getColour() << endl;
		cout << "\tSex: " << foundBunny->getSex() << endl << endl;
	}
	else {
		cout << "No bunny with ID " << bunnyID << endl << endl;
	}
}

