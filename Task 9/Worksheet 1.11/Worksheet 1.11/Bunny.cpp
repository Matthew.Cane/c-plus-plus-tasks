#include "Bunny.h"

Bunny::Bunny(int bunnyID, Bunny *nextBunny_p) {

	string names[10] = {"Peter", "Flopsy", "Tigger", "Snowball", "Dash",
						 "Fluffy", "Whiskers", "Bob", "Jerry", "Fred"};
	string sexes[2] = {"Male", "Female"};
	string colours[4] = { "White", "Brown", "Black", "Spotted" };

	this->bunnyID = bunnyID;
	sex = sexes[rand() % 2];
	colour = colours[rand() % 4];
	age = rand() % 11;
	name = names[rand() % 10];
	nextBunny = nextBunny_p;

}

int Bunny::getID() {
	return bunnyID;
}

string Bunny::getSex() {
	return sex;
}

string Bunny::getColour() {
	return colour;
}

int Bunny::getAge() {
	return age;
}

string Bunny::getName() {
	return name;
}

void Bunny::setNext(Bunny *next) {
	nextBunny = next;
}

Bunny* Bunny::getNext() {
	return nextBunny;
}