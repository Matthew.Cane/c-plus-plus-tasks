#include "stdafx.h"
#include "wheel.h"
#include <string>
using std::string;

wheel::wheel(string pinSet, int pinSetting)
{
	for(int i=0;i<pinSet.length();i++)
	{
		pins.push_back(pinSet[i]-'0'); //Added -'0'
	}
	this->pinSetting=pinSetting; //Removed ->this
}

void wheel::rotate()
{
	if (++pinSetting == pins.size()) pinSetting = 0; //Changed < to ==
}

int wheel::getCurrentPin()
{
	return pins[pinSetting]; //BUG HERE, VECTOR OUT OF BOUNDS, MAYBE FIXED
}