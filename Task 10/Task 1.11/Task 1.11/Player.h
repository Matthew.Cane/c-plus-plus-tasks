#pragma once
#include <vector>
#include <iostream>
#include <string>
#include <Windows.h>
#include "Card.h"
using namespace std;

class Player {
private:
	string name;
	int points = 0;
	vector<Card> hand;

public:
	Player() {}
	Player(string newName);
	
	Card playCard();
	bool hasCards();
	int getPoints();
	string getName();
	void setPoints(int newPoints);
	void printHand();
	void putInDeck(Card card);
};