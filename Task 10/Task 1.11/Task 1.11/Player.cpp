#include "Player.h"

Player::Player(string newName) {
	name = newName;
}

Card Player::playCard() {
	Card playedCard = hand.back();
	cout << name << " is playing " << "The " << playedCard.getRank() << " of " << playedCard.getSuit() << endl;
	Sleep(250);
	hand.pop_back();
	return playedCard;
}

bool Player::hasCards() {
	if (hand.size() == 0) {
		return false;
	}
	return true;
}

int Player::getPoints() { return points; }

string Player::getName() {
	return name;
}

void Player::setPoints(int newPoints) {	points = newPoints; }

void Player::printHand() {
	cout << "Printing " << name << "'s Hand:\n\n";
	for (unsigned int i = 0; i < hand.size(); i++) {
		hand[i].printCard();
	}
	cout << endl;
}

void Player::putInDeck(Card card) {
	hand.push_back(card);
}
