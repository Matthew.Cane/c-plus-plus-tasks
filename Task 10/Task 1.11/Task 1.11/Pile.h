#pragma once
#include <vector>
#include <iostream>
#include <cstdlib>
#include "Card.h"
#include "Player.h"
using namespace std;

class Pile {
private:
	vector<Card> pile;
	vector<Player*> playerVector;
public:
	Pile() {}
	Pile(vector<Player*> players);

	void putOnPile(Card card);
	void printPile();
	void snap();
};