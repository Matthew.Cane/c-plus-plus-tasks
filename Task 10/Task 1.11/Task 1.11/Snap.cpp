#include "Snap.h"

void Snap::playSnap() {
	createPlayers();
	createDeck();
	dealCards();
	playCards();
	calculateScore();
}

void Snap::createPlayers() {
	cout << "Enter the number of players (1-5): ";
	cin >> playerCount;

	for (int i = 0; i < playerCount; i++) { //Create players
		Player* player = new Player(names[i]);
		playerVector.push_back(player);
	}
}

void Snap::createDeck() {
	Deck deck = Deck();

	Pile pile = Pile(playerVector);
	deck.shuffleDeck();

	cardDeck = deck;
	cardPile = pile;
}

void Snap::dealCards() {
	while (cardDeck.cardsInDeck() > playerCount) { //Dealing cards
		for (unsigned int i = 0; i < playerVector.size(); i++) {
			playerVector[i]->putInDeck(cardDeck.dealCard());
		}
	}
}

void Snap::playCards() {
	while (cardCheck == true) { //Playing cards
		for (unsigned int i = 0; i < playerVector.size(); i++) {
			if (playerVector[i]->hasCards()) {
				cardPile.putOnPile(playerVector[i]->playCard());
				cardCheck = true;
				continue;
			}
			cardCheck = false;

		}
	}
}

void Snap::calculateScore() {
	int pointsTotal = 0;
	//string winner = "Nobody";
	vector<Player*> winners;

	for (int i = 0; i < playerCount; i++) { //Count up points
		if (playerVector[i]->getPoints() > pointsTotal) {
			pointsTotal = playerVector[i]->getPoints();
			//winner = playerVector[i]->getName();
		}
	}

	for (int i = 0; i < playerCount; i++) { //Count up points
		if (playerVector[i]->getPoints() == pointsTotal) {
			winners.push_back(playerVector[i]);
		}
	}

	cout << "\n The game is over!" << endl;

	if (pointsTotal == 0) {
		cout << "The game ended with a draw, everybody is a winner!" << endl;
		return;
	}

	if (winners.size() == 1) {
		cout << "The winner with " << pointsTotal << " points is "
			<< winners[0]->getName() << endl;
	}
	else {
		cout << "The joint winners with " << pointsTotal << " points are ";
		for (unsigned int i = 0; i < winners.size(); i++) {
			if (i == winners.size() - 1) {
				cout << winners[i]->getName() << endl;
				return;
			}
			cout << winners[i]->getName() << " and ";
		}
	}
}
