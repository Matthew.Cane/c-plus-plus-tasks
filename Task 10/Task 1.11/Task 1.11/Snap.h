#pragma once
#include "Card.h"
#include "Deck.h"
#include "Pile.h"
#include "Player.h"

class Snap {
private:
	int playerCount;
	bool cardCheck = true;
	vector<Player*> playerVector;
	string names[5] = { "Matt", "Bella", "Emily", "Steve", "Frank" };
	Pile cardPile;
	Deck cardDeck;

public:
	void playSnap();
	void createPlayers();
	void createDeck();
	void dealCards();
	void playCards();
	void calculateScore();
};