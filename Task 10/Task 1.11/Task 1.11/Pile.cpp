#include "Pile.h"

Pile::Pile(vector<Player*> players) {
	playerVector = players;
}

void Pile::putOnPile(Card card) {
	if (pile.size() != 0) {
		Card topCard = pile.back();
		if (topCard.getRank() == card.getRank()) {
			snap();
		}
	}
	pile.push_back(card);
}

void Pile::printPile() {
	cout << "printing Pile:\n\n";
	for (unsigned int i = 0; i < pile.size(); i++) {
		pile[i].printCard();
	}
	cout << endl;
}

void Pile::snap() {
	int randint = rand() % playerVector.size();
	Player *winner = playerVector[randint];
	winner->setPoints(winner->getPoints() + 1);
	cout << endl << winner->getName() << " has called SNAP!"
		<< " and is awarded 1 point." << endl
		<< "They now have " << winner->getPoints()
		<< " points" << endl << endl;
	
}
