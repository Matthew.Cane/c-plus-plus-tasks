#include "Deck.h"
#include "CardValues.h"

Deck::Deck() {
	srand((unsigned int)time(0));
	CardValues values;
	for (int x = 0; x < 4; x++) {
		for (int z = 1; z < 14; z++) {
			Card card = Card(values.getSuitValue(x), values.getRankValue(z));
			deck.push_back(card);
		}
	}
}

void Deck::shuffleDeck() {
	random_shuffle(deck.begin(), deck.end());
}

Card Deck::dealCard() {

	Card dealtCard = deck.back();
	deck.pop_back();
	return dealtCard;
}

bool Deck::hasCard() {
	if (deck.size() == 0) {
		return false;
	}
	return true;
}

void Deck::printDeck() {
	cout << "Printing Deck:\n\n";
	for (unsigned int i = 0; i < deck.size(); i++) {
		deck[i].printCard();
	}
}

int Deck::cardsInDeck() {
	return deck.size();
}
