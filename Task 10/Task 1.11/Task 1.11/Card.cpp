#include "Card.h"
#include "CardValues.h"

Card::Card(string newSuit, string newRank) {
	suit = newSuit;
	rank = newRank;
}

void Card::printCard() {
	//cout << "\tCard Suit: " << suit << "\n\tCard Rank: " << rank << endl;
	cout << "The " << rank << " of " << suit << endl;
}

string Card::getSuit() {
	return suit;
}

string Card::getRank()
{
	return rank;
}
