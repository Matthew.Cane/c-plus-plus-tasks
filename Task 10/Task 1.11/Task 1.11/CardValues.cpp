#include "CardValues.h"

CardValues::CardValues() {
	suitValues[0] = "Diamonds";
	suitValues[1] = "Clubs";
	suitValues[2] = "Hearts";
	suitValues[3] = "Spades";

	rankValues[1] = "One";
	rankValues[2] = "Two";
	rankValues[3] = "Three";
	rankValues[4] = "Four";
	rankValues[5] = "Five";
	rankValues[6] = "Six";
	rankValues[7] = "Seven";
	rankValues[8] = "Eight";
	rankValues[9] = "Nine";
	rankValues[10] = "Ten";
	rankValues[11] = "Jack";
	rankValues[12] = "Queen";
	rankValues[13] = "King";
}

string CardValues::getSuitValue(int suit) {
	map<int, string>::iterator it;
	it = suitValues.find(suit);
	if (it == suitValues.end()) {
		return "";
	}
	return it->second;
}

string CardValues::getRankValue(int rank) {
	map<int, string>::iterator it;
	it = rankValues.find(rank);
	if (it == rankValues.end()) {
		return "";
	}
	return it->second;
}
