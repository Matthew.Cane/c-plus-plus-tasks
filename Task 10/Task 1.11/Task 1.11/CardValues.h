#pragma once
#include <map>
#include <string>
using namespace std;

class CardValues {
private:
	map<int, string> suitValues;
	map<int, string> rankValues;

public:
	CardValues();

	string getSuitValue(int suit);
	string getRankValue(int rank);
};