#pragma once
#include <string>
#include <iostream>
using namespace std;

class Card {
private:
	string suit;
	string rank;

public:
	Card(string newSuit, string newRank);
	void printCard();
	string getSuit();
	string getRank();

};