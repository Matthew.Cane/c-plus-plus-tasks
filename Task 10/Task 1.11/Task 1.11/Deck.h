#pragma once
#include <map>
#include <string>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <algorithm>
#include "Card.h"
#include "Player.h"
using namespace std;

class Deck {
private:
	vector<Card> deck;
public:
	
	Deck();
	void shuffleDeck();
	Card dealCard();
	bool hasCard();
	void printDeck();
	int cardsInDeck();
};