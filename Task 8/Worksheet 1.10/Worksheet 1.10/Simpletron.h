#pragma once
#include <map>
#include <vector>
#include <string>
#include <iostream>
#include <iomanip>
#include <stdexcept>
using namespace std;

struct deconstructed {
	int code;
	int instruction;
	int value;
};

class Simpletron {
	vector<deconstructed> programMemory;
	map<int, string> instructionSet;
public:
	Simpletron();

	void loadProgramIntoMemory(int* program);
	void disassembleProgram();
};