#include "Simpletron.h"

Simpletron::Simpletron() {
	instructionSet[10] = "READ into xx";
	instructionSet[11] = "WRITE xx to CONSOLE ";
	instructionSet[20] = "LOAD xx into Acc";
	instructionSet[21] = "STORE Acc into xx";
	instructionSet[30] = "ADD xx";
	instructionSet[31] = "SUB xx";
	instructionSet[32] = "MUL by xx";
	instructionSet[33] = "DIV by xx";
	instructionSet[40] = "BRANCH into xx";
	instructionSet[41] = "BRANCH IFNEG to xx";
	instructionSet[42] = "BRANCH IFZERO to xx";
	instructionSet[43] = "HALT";
}

void Simpletron::loadProgramIntoMemory(int* program) {
	int code = 0; 
	int value, instruction;
	deconstructed info;

	for (int i = 0; code != 4300; i++) {
		code = *(program + i);
		instruction = code / 100;
		value = code % 100;
		
		info.code = code;
		info.instruction = instruction;
		info.value = value;

		programMemory.push_back(info);
	}
}

void Simpletron::disassembleProgram() {
	deconstructed info;
	string descript;
	string value_s;

	for (int i = 0; i < programMemory.size(); i++) {
		info = programMemory[i];

		cout << setfill('0') << setw(2) << i << " : ";
		cout << "(" << info.code << ") ";
		
		try {
			descript = instructionSet.at(info.instruction);
		}
		catch (out_of_range) {
			cout << "Invalid instruction at instruction number: " << i << endl;
			exit(1);
		}

		if (info.instruction != 43) {
			value_s = to_string(info.value);
			if (value_s.length() == 1) {
				value_s.insert(0, "0");
			}

			int xxpos = static_cast<int>(descript.find("xx"));
			descript.replace(xxpos, 2, value_s);
		}
		cout << descript << endl;
	}
}