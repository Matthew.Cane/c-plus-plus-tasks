#include <iostream>
#include "Simpletron.h"

int main()
{
	Simpletron simpletron = Simpletron();
	
	int program [7] = {1007, 1008, 2007, 3008, 2109, 1109, 4300};

	int* program_p = &program[0];

	simpletron.loadProgramIntoMemory(program_p);

	simpletron.disassembleProgram();

	return 0;
}
