#pragma once
#include <string>
#include <vector>
#include "UserRating.h"
#include "UserComment.h"
using namespace std;


class Movie {
private:
	string title;
	int year;
	string desc;
	string genre;

	vector<UserRating> ratings;
	vector<UserComment> comments;

public:
	UserRating addMovieRating(int);
	UserComment addComment(string);

	void setTitle(string newTitle);
	void setYear(int newYear);
	void setDesc(string newDesc);
	void setGenre(string newGenre);

	string getTitle();
	int getYear();
	string getDesc();
	string getGenre();

	Movie(string newTitle, int newYear, string newDesc, string newGenre);
};