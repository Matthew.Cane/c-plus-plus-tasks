#include "UserData.h"

UserData::UserData() {

}

UserData::UserData(string newName, string newAddress, int newCreditCard) {
	name = newName;
	address = newAddress;
	creditCard = newCreditCard;
}

void UserData::setName(string newName) {
	name = newName;
};
void UserData::setAddress(string newAddress) {
	address = newAddress;
};
void UserData::setCreditCard(int newCreditCard) {
	creditCard = newCreditCard;
};
string UserData::getName() {
	return name;
};
string UserData::getAddress() {
	return address;
};
int UserData::getCreditCard() {
	return creditCard;
};