#pragma once
#include <string>
#include <vector>
#include "movie.h"
#include "UserData.h"
using namespace std;

class RegisteredUser {
private:
	string email;
	string password;
	
	vector<Movie> movies;
	UserData userData;

	
public:
	Movie addMovie(string newTitle, int newYear, string newDesc, string newGenre);
	void addUserData(string newName, string newAddress, int newCreditCard);
	
	string getEmail();
	string getPassword();
	
	string setEmail(string newEmail);
	string setPassword(string newPassword);

	RegisteredUser(string email, string password);
	RegisteredUser();
};