#pragma once

class UserRating {
private:
	int vote;

public:
	int getVote();
	UserRating(int newVote);
};