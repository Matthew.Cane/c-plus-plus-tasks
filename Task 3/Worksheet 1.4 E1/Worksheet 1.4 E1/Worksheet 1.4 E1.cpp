#include "RegisteredUser.h"
#include "Theater.h"
#include "Movie.h"
#include "UserRating.h"
#include "UserComment.h"
#include "UserData.h"

int main()
{
	// create a new theatre object
	Theater uweTheatre("UWE Frenchay");

	// register a user with the theatre and add some user details
	RegisteredUser user1 = uweTheatre.RegisterUser("user1@here.com", "password");
	user1.addUserData("user1", "bristol", 1234);

	// add a favourite movie for the user1
	Movie fightClub = user1.addMovie("Fight Club", 1999, "An insomniac office worker crosses paths with a devil-may-care soap maker forming an underground fight club", "Drama");

	// add a comment about and a rating for the Fight Club movie
	UserComment comment1 = fightClub.addComment("What a cool film with an suprising twist!");
	fightClub.addMovieRating(5);

	// increment the number of times the comment has been read and give it a thumbs up
	comment1.incrementReads();
	comment1.incrementThumbsUp();

	return 0;
}