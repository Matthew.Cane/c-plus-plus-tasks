#include "UserRating.h"
#include "UserComment.h"

UserComment::UserComment(string newComment) {
	comment = newComment;
}

void UserComment::incrementReads() {
	reads = reads + 1;
}

void UserComment::incrementThumbsUp() {
	thumbsUp = thumbsUp + 1;
}

string UserComment::getComment() {
	return comment;
}

int UserComment::getReads() {
	return reads;
}

int UserComment::getThumbsUp() {
	return thumbsUp;
}
