#pragma once
#include <string>
using namespace std;

class UserComment {
private:
	string comment;
	int reads = 0;
	int thumbsUp = 0;

public:
	UserComment(string newComment);

	void incrementReads();
	void incrementThumbsUp();

	string getComment();
	int getReads();
	int getThumbsUp();
};