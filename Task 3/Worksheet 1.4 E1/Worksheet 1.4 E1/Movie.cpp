#include "Movie.h"
#include "UserComment.h"

UserRating Movie::addMovieRating(int newRating) {
	UserRating rating = UserRating(newRating);
	ratings.push_back(rating);
	return rating;
};
UserComment Movie::addComment(string newComment) {
	UserComment comment = UserComment(newComment);
	comments.push_back(comment);
	return comment;
};
void Movie::setTitle(string newTitle) {
	title = newTitle;
};
void Movie::setYear(int newYear) {
	year = newYear;
};
void Movie::setDesc(string newDesc) {
	desc = newDesc;
};
void Movie::setGenre(string newGenre) {
	genre = newGenre;
};
string Movie::getTitle() {
	return title;
};
int Movie::getYear() {
	return year;
};
string Movie::getDesc() {
	return desc;
};
string Movie::getGenre() {
	return genre;
}
Movie::Movie(string newTitle, int newYear, string newDesc, string newGenre) {
	title = newTitle;
	year = newYear;
	desc = newDesc;
	genre = newGenre;
}
;