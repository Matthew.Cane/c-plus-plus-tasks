#pragma once
#include <string>
#include <vector>
#include "RegisteredUser.h"
using namespace std;

class Theater {
private:
	string address;
	vector<RegisteredUser> users;

public:
	RegisteredUser RegisterUser(string email, string password);
	void setAddress(string address);
	string getAddress();
	Theater(string Name);
};