#include "RegisteredUser.h"
#include "UserData.h"

Movie RegisteredUser::addMovie(string newTitle, int newYear, string newDesc, string newGenre) {
	Movie newMovie = Movie(newTitle, newYear, newDesc, newGenre);
	movies.push_back(newMovie);
	return newMovie;
};
void RegisteredUser::addUserData(string newName, string newAddress, int newCreditCard) {
	userData = UserData(newName, newAddress, newCreditCard);
};
string RegisteredUser::getEmail() {
	return email;
};
string RegisteredUser::getPassword() {
	return password;
};
string RegisteredUser::setEmail(string newEmail) {
	email = newEmail;
	return email;
};
string RegisteredUser::setPassword(string newPassword) {
	password = newPassword;
	return password;
};

RegisteredUser::RegisteredUser(string newEmail, string newPassword) {
	email = newEmail;
	password = newPassword;
};