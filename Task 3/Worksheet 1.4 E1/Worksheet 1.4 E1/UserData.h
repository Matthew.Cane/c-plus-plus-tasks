#pragma once
#include <string>
using namespace std;

class UserData {
private:
	string name;
	string address;
	int creditCard;

public:
	UserData(string newName, string newAddress, int newCreditCard);
	UserData();

	void setName(string newName);
	void setAddress(string newAddress);
	void setCreditCard(int newCreditCard);

	string getName();
	string getAddress();
	int getCreditCard();
};