#pragma once
#include <iostream>
#include "CarbonFootprint.h"
using namespace std;

class Bicycle : public CarbonFootprint {
private:
	string type;
	int wheelSize;
	int sproketTeeth;
	int gearCount;
public:
	Bicycle();

	string getType();
	int getWheelSize();
	int getSproketTeeth();
	int getGearCount();

	void setType(string newType);
	void setWheelSize(int newWheelSize);
	void setSproketTeeth(int newSproketTeeth);
	void setGearCount(int newGearCount);

	virtual float getCarbonFootprint();
};