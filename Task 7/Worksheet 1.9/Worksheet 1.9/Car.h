#pragma once
#include <iostream>
#include "CarbonFootprint.h"
using namespace std;

class Car : public CarbonFootprint {
private:
	int weight;
	int doors;
	string manufacturer;
	float engineSize;
public:
	Car();

	int getWeight();
	int getDoors();
	string getManufacturer();
	float getEngineSize();

	void setWeight(int newWeight);
	void setDoors(int newDoors);
	void setManufacturer(string newManufacturer);
	void setEngineSize(float newEngineSize);

	virtual float getCarbonFootprint();
};