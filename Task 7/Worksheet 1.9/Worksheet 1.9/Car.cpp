#include "Car.h"

Car::Car() {
	weight = 0;
	doors = 0;
	manufacturer = "none";
	engineSize = 1.2;
}

int Car::getWeight() {
	return weight;
}

int Car::getDoors() {
	return doors;
}

string Car::getManufacturer() {
	return manufacturer;
}

float Car::getEngineSize() {
	return engineSize;
}

void Car::setWeight(int newWeight) {
	weight = newWeight;
}

void Car::setDoors(int newDoors) {
	doors = newDoors;
}

void Car::setManufacturer(string newManufacturer) {
	manufacturer = newManufacturer;
}

void Car::setEngineSize(float newEngineSize) {
	engineSize = newEngineSize;
}

float Car::getCarbonFootprint() {
	return 68.5;
}
