#pragma once

class CarbonFootprint {
public:
	virtual float getCarbonFootprint() = 0;
};