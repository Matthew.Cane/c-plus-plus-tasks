#include "Bicycle.h"

Bicycle::Bicycle() {
	type = "none";
	wheelSize = 0;
	sproketTeeth = 0;
	gearCount = 0;
}

string Bicycle::getType() {
	return type;
}

int Bicycle::getWheelSize() {
	return wheelSize;
}

int Bicycle::getSproketTeeth() {
	return sproketTeeth;
}

int Bicycle::getGearCount() {
	return gearCount;
}

void Bicycle::setType(string newType) {
	type = newType;
}

void Bicycle::setWheelSize(int newWheelSize) {
	wheelSize = newWheelSize;
}

void Bicycle::setSproketTeeth(int newSproketTeeth) {
	sproketTeeth = newSproketTeeth;
}

void Bicycle::setGearCount(int newGearCount) {
	gearCount = newGearCount;
}

float Bicycle::getCarbonFootprint() {
	return 0.13;
}