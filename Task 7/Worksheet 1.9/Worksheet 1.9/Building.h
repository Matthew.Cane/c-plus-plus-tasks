#pragma once
#include "CarbonFootprint.h"
using namespace std;

class Building : public CarbonFootprint {
private:
	int EER;
	int floorSpace;
	int floors;
	int windows;
public:
	Building();

	int getEER();
	int getfloorSpace();
	int getFloors();
	int getWindows();

	void setEER(int newEER);
	void setFloorSpace(int newFloorSpace);
	void setFloors(int newFloors);
	void setWindows(int newWindows);

	virtual float getCarbonFootprint();
};