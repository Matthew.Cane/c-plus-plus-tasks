#include "Building.h"

Building::Building() {
	EER = 0;
	floorSpace = 0;
	floors = 0;
	windows = 0;
}

int Building::getEER() {
	return EER;
};
int Building::getfloorSpace() {
	return floorSpace;
};
int Building::getFloors() {
	return floors;
};
int Building::getWindows() {
	return windows;
};

void Building::setEER(int newEER) {
	EER = newEER;
};
void Building::setFloorSpace(int newFloorSpace) {
	floorSpace = newFloorSpace;
};
void Building::setFloors(int newFloors) {
	floors = newFloors;
};
void Building::setWindows(int newWindows) {
	windows = newWindows;
};

float Building::getCarbonFootprint() {
	return 17.1;
};