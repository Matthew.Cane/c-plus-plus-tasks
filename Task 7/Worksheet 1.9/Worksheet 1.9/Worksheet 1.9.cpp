#include <iostream>
#include <vector>
#include "Bicycle.h"
#include "Car.h"
#include "Building.h"
#include "CarbonFootprint.h"
using namespace std;

int main()
{
	vector<CarbonFootprint*> carbonVector;

	Bicycle bike = Bicycle();
	Car car = Car();
	Building house = Building();

	CarbonFootprint *bike_p = &bike;
	CarbonFootprint *car_p = &car;
	CarbonFootprint *house_p = &house;

	carbonVector.push_back(bike_p);
	carbonVector.push_back(car_p);
	carbonVector.push_back(house_p);

	for (int i = 0; i < carbonVector.size(); ++i) {
		CarbonFootprint *obj = carbonVector.at(i);
		cout << obj->getCarbonFootprint() << endl;
	}
}