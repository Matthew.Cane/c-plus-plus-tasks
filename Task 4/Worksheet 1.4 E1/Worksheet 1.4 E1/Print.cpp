#include <iostream>
#include <vector>
#include "print.h"
#include "Map.h"

using std::cout;
using std::endl;
using std::vector;

void Print::printMap(Map gameMap) {
	system("CLS");
	for (int h = gameMap.mapHeight - 1; h >= 0; h--) {
		for (int w = 0; w < gameMap.mapWidth; w++) {
			cout << gameMap.map[w][h];
		}

		cout << endl;
	}
}