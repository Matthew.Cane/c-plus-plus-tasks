#pragma once
#include <vector>

using std::vector;

class Map {
public:
	vector<vector<char>> map;
	int playerLocation[2]; //pl[0] is width, pl[1] is height
	int mapHeight;
	int mapWidth;
	
	Map(int mapHeight, int mapWidth, int traps);

};