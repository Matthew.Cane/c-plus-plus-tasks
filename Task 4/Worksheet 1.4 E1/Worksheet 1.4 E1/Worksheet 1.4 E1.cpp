#include <iostream>
#include <string>
#include "Map.h"
#include "print.h"
#include "Move.h"

using std::cout;
using std::cin;
using std::endl;
using std::string;

#define MAP_WIDTH 20
#define MAP_HEIGHT 6
#define TRAP_NUMBER 10

int main()
{
	//Instanciates map object of given dimentions
	Map gameMap(MAP_HEIGHT, MAP_WIDTH, TRAP_NUMBER);
	
	//Instanciates objects for printing and moving
	Print printMap;
	Move movePlayer;	

	bool gamePlaying = true;

	while (gamePlaying) {
		//Prints the map
		printMap.printMap(gameMap);

		cout << "Enter move (w, a, s, d): ";
		char in[10];
		cin >> in;

		char direction = in[0];

		if (direction != 'w' && direction != 'a' && direction != 's' && direction != 'd') {
			cout << "Invalid move, try again" << endl;
			continue;
		}

		//Returns 0 if moved, 1 if fail, 2 if win
		int state = movePlayer.movePlayer(gameMap, direction);

		if (state == 0) {
			continue;
		}
		else if (state == 1) {
			cout << "You have hit a trap! Game over." << endl;
			gamePlaying = false;
		}
		else if (state == 2) {
			cout << "You have found the treasure and won the game! Congradulations." << endl;
			gamePlaying = false;
		}
		else {
			cout << "***Internal game error***" << endl;
			gamePlaying = false;
		}
	}
}