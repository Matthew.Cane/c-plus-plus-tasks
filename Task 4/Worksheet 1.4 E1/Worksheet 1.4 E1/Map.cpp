#include <vector>
#include <cstdlib> 
#include <ctime>
#include <iostream>
#include "Map.h"

using std::vector;
using std::rand;
using std::time;

Map::Map(int gameHeight, int gameWidth, int traps) {
	vector<char> mapy(gameHeight, '.');
	
	for (int i = 0; i < gameWidth; i++) {
		map.push_back(mapy);
	}

	mapHeight = gameHeight;
	mapWidth = gameWidth;

	//Seeds random number
	srand(time(NULL));

	//Adds the given number of traps randomly
	for (int i = 0; i < traps; i++) {
		map[rand() % mapWidth][rand() % mapHeight] = 'T';
	}

	//Randomly places treasure
	bool check = false;
	while (check == false) {
		int tmprW = rand() % mapWidth;
		int tmprH = rand() % mapHeight;
		
		if (map[tmprW][tmprH] == '.') {
			map[tmprW][tmprH] = 'X';
			check = true;
		}
	}

	check = false;
	while (check == false) {
		int tmprW = rand() % mapWidth;
		int tmprH = rand() % mapHeight;

		if (map[tmprW][tmprH] == '.') {
			int pw = rand() % mapWidth;
			int ph = rand() % mapHeight;

			playerLocation[0] = pw;
			playerLocation[1] = ph;

			map[pw][ph] = '@';
			check = true;
		}
	}

}