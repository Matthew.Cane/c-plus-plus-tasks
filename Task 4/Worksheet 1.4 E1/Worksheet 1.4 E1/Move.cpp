#include "Move.h"
#include "Map.h"
#include <iostream>
using std::cout;
using std::endl;

int Move::movePlayer(Map &gameMap, char direction) {
	//Returns 0 if moved, 1 if fail, 2 if win
	
	char nextChar = '!';
	int nextX = 0;
	int nextY = 0;

	if (direction == 'w') {
		nextX = gameMap.playerLocation[0];
		nextY = gameMap.playerLocation[1] + 1;
		if (!(nextY == gameMap.mapHeight)) {
			nextChar = gameMap.map[gameMap.playerLocation[0]][gameMap.playerLocation[1] + 1];
		}
	}
	else if (direction == 'a') {
		nextX = gameMap.playerLocation[0] - 1;
		nextY = gameMap.playerLocation[1];
		if (!(nextX < 0)) {
			nextChar = gameMap.map[gameMap.playerLocation[0] - 1][gameMap.playerLocation[1]];
		}
	}
	else if (direction == 's') {
		nextX = gameMap.playerLocation[0];
		nextY = gameMap.playerLocation[1] - 1;
		if (!(nextY < 0)) {
			nextChar = gameMap.map[gameMap.playerLocation[0]][gameMap.playerLocation[1] - 1];
		}
	}
	else if (direction == 'd') {
		nextX = gameMap.playerLocation[0] + 1;
		nextY = gameMap.playerLocation[1];
		if (!(nextX == gameMap.mapWidth)) {
			nextChar = gameMap.map[gameMap.playerLocation[0] + 1][gameMap.playerLocation[1]];
		}
	}
	//cout << "Next position: " << nextChar << endl; //Debug

	if (nextChar == 'T') {
		return 1;
	}
	else if (nextChar == 'X') {
		return 2;
	}
	else if (nextChar == '.') {
		gameMap.map[nextX][nextY] = '@';
		gameMap.map[gameMap.playerLocation[0]][gameMap.playerLocation[1]] = '.';
		gameMap.playerLocation[0] = nextX;
		gameMap.playerLocation[1] = nextY;
		return 0;
	}
	else {
		if (nextY < 0) {
			//Too low
			if (gameMap.map[nextX][gameMap.mapHeight - 1] == 'T') {
				return 1;
			}
			else if (gameMap.map[nextX][gameMap.mapHeight - 1] == 'X') {
				return 2;
			}
			gameMap.map[nextX][gameMap.mapHeight - 1] = '@';
			gameMap.map[gameMap.playerLocation[0]][gameMap.playerLocation[1]] = '.';
			gameMap.playerLocation[0] = nextX;
			gameMap.playerLocation[1] = gameMap.mapHeight - 1;
			return 0;
		}
		else if (nextY == gameMap.mapHeight) {
			//Too high
			if (gameMap.map[nextX][0] == 'T') {
				return 1;
			}
			else if (gameMap.map[nextX][0] == 'X') {
				return 2;
			}
			gameMap.map[nextX][0] = '@';
			gameMap.map[gameMap.playerLocation[0]][gameMap.playerLocation[1]] = '.';
			gameMap.playerLocation[0] = nextX;
			gameMap.playerLocation[1] = 0;
			return 0;
		}
		else if (nextX < 0) {
			//Too far left
			if (gameMap.map[gameMap.mapWidth - 1][nextY] == 'T') {
				return 1;
			}
			else if (gameMap.map[gameMap.mapWidth - 1][nextY] == 'X') {
				return 2;
			}
			gameMap.map[gameMap.mapWidth - 1][nextY] = '@';
			gameMap.map[gameMap.playerLocation[0]][gameMap.playerLocation[1]] = '.';
			gameMap.playerLocation[0] = gameMap.mapWidth - 1;
			gameMap.playerLocation[1] = nextY;
			return 0;
		}
		else if (nextX == gameMap.mapWidth) {
			//Too far right
			if (gameMap.map[0][nextY] == 'T') {
				return 1;
			}
			else if (gameMap.map[0][nextY] == 'X') {
				return 2;
			}
			gameMap.map[0][nextY] = '@';
			gameMap.map[gameMap.playerLocation[0]][gameMap.playerLocation[1]] = '.';
			gameMap.playerLocation[0] = 0;
			gameMap.playerLocation[1] = nextY;
			return 0;
		}
	}
}